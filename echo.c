#include <stdio.h>
#include <stdbool.h>
#include <string.h>

int main(int argc, char** argv){
    bool n = false, e = false, E = false;
    int i = 1;
    while(i < argc){
        if(!strcmp(argv[i], "-n")){
            n = true;
        }
        else if(!strcmp(argv[i], "-e")){
            e = true;
        }
        else if(!strcmp(argv[i], "-E")){
            E = true;
        }
        i++;
    }
    for (i; i < argc; ++i){
        printf("%s ", argv[i]);
    }
    if(!n)
        printf("\n");
    
}