#include <stdio.h>
#include <dirent.h>
#include <errno.h>
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>
#include <pwd.h>
#include <grp.h>
#include <time.h>

void _ls(const char* dir, int op_a, int op_l){
    struct dirent *d;
    struct stat thestat;
    struct passwd *tf;
    struct group *gf;
    char buf[512];
    DIR *dh = opendir(dir);


    if(!dh){
        if(errno = ENOENT){
            perror("Dirrectory does not exist\n");
        }
        else{
            perror("Unable to read direcotry\n");
        }
        exit(EXIT_FAILURE);
    }
    while((d = readdir(dh)) != NULL){
        if(!op_a && d->d_name[0] == '.') 
            continue;
        if(op_l){
            sprintf(buf, "%s", d->d_name);
            stat(buf, &thestat);
            //permission
            printf( (thestat.st_mode & S_IRUSR) ? "-r" : " -");
            printf( (thestat.st_mode & S_IWUSR) ? "w" : "-");
            printf( (thestat.st_mode & S_IXUSR) ? "x" : "-");
            printf( (thestat.st_mode & S_IRGRP) ? "r" : "-");
            printf( (thestat.st_mode & S_IWGRP) ? "w" : "-");
            printf( (thestat.st_mode & S_IXGRP) ? "x" : "-");
            printf( (thestat.st_mode & S_IROTH) ? "r" : "-");
            printf( (thestat.st_mode & S_IWOTH) ? "w" : "-");
            printf( (thestat.st_mode & S_IXOTH) ? "x" : "-");
            //number of hard links
            printf(" %ld ", thestat.st_nlink);
            //user name
            tf = getpwuid(thestat.st_uid);
            printf("%s ", tf->pw_name);
            //group
            gf = getgrgid(thestat.st_gid);
            printf("%s ", gf->gr_name);
            //file size
            printf("%zu ",thestat.st_size);
            //file name
            printf("%s ", d->d_name);
            //time of last modification
            printf("%s ", ctime(&thestat.st_mtime));
        }else{
            printf("%s ", d->d_name);
        }
    }
    if(!op_l)
        printf("\n");
    closedir(dh);
}

int main(int argc, char** argv){
    if(argc == 1){
        _ls(".", 0, 0);
    }
    else if(argc == 2){
        if(argv[1][0] == '-'){
            int op_a = 0, op_l = 0;
            char *p = (char *)(argv[1] + 1);
            while(*p){
                if(*p == 'a') op_a = 1;
                else if(*p == 'l') op_l = 1;
                else{
                    perror("Unkown option error");
                    exit(EXIT_FAILURE);
                }
                p++;
            }
            _ls(".", op_a, op_l);

        } else{
            _ls(argv[1], 0, 0);
        }
    } else if(argc == 3){
        if (argv[1][0] != '-'){
            perror("Incorrect Usage error");
            exit(EXIT_FAILURE);
        }
        int op_a = 0, op_l = 0;
        char *p = (char *)(argv[1] + 1);
        while(*p){
            if(*p == 'a') op_a = 1;
            else if(*p == 'l') op_l = 1;
            else{
                perror("Unkown option error");
                exit(EXIT_FAILURE);
            }
            p++;
        }
        _ls(argv[2], op_a, op_l);
    }
    

    return 0;
}