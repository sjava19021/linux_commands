#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>

int main(int argc, char** argv){
    bool word = false, line = false, byte = false, file = false; 
    int file_count = 0;
    for(int i = 1; i < argc; ++i){
        if(!strcmp(argv[i], "-w")){
            word = true;
        }
        else if(!strcmp(argv[i], "-c")){
            byte = true;
        }
        else if(!strcmp(argv[i], "-l")){
            line = true;
        }
        else{
            file = true;
            file_count++;
        }
    }
    if (!file){
            fprintf(stderr, "Usage: %s file1 [other_files] [-flags]\n", argv[0]);
            exit(EXIT_FAILURE);
    }
    int total_new_line = 0, total_words = 0, total_byte_count = 0;
    for(int i = 1; i < argc; ++i){
        int new_line = 0, words = 0, byte_count = 0;
        char c, prev_c;
        if(!strcmp(argv[i], "-w") || !strcmp(argv[i], "-c") || !strcmp(argv[i], "-l")){
            continue;
        }
        FILE* f = fopen(argv[i], "r");
        if(!f){
            fprintf(stderr, "%s: %s: No such file or directory\n", argv[0], argv[i]);
            exit(EXIT_FAILURE);
        }
        c = fgetc(f);
        while(c != EOF){
            byte_count++;
            if(c == '\n'){
                new_line++;
            }
            prev_c = c;
            c = fgetc(f);
            if((c == ' ' || c == EOF || c == '\n') && prev_c != ' '){
                words++;
            }
        }
        total_byte_count += byte_count;
        total_new_line += new_line;
        total_words += words;
        printf("%d %d %d %s\n", new_line, words, byte_count, argv[i]);
    }
    if(file_count > 1){
        printf("%d %d %d %s\n", total_new_line, total_words, total_byte_count, "total");
    }
}